"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""


def count_odd_num(n):
    a = 0
    if type(n) != int:
        return 'Must be int!'
    elif n <= 0:
        return "Must be > 0!"

    else:
        for i in str(n):
            if int(i) % 2 == 1:
                a += 1
        return a
